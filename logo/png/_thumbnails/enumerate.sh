#!/bin/bash

SRC_DIR=$(readlink -f "$(dirname "$0")")

function label_file()
{
    local file="$1"
    local i="$2"
    echo "Converting $1"
    convert "$file" -gravity northeast -append -fill red \
     -font Courier-Bold -pointsize 30 -annotate +0+20 " $i " "${SRC_DIR}/labeled_$file"
}

number=1
for i in $(ls -v "${SRC_DIR}"/[0-9]*.png)
do
    label_file "$(basename "$i")" $((number++))
done
